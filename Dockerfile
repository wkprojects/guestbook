FROM maven:3-jdk-11 as build

COPY . /app
WORKDIR /app

RUN mvn clean install

FROM alpine:latest as unzip

RUN mkdir -p /app/docker /app/src
WORKDIR app
RUN apk add --no-cache unzip
COPY --from=build /app/target /app/src
RUN  unzip -q $(find /app/src \( -maxdepth 1 -iname '*.jar' ! -iname '*sources.jar' \) | head -n 1) -d /app/docker && ls -al /app/docker

FROM amazoncorretto:11

EXPOSE 8080

WORKDIR /app

ENTRYPOINT ["/bin/bash", "-c"]
CMD ["java -Djava.security.egd=file:/dev/urandom -Dspring.profiles.active=$SPRING_ACTIVE_PROFILE org.springframework.boot.loader.JarLauncher"]

COPY --from=unzip /app/docker/BOOT-INF/lib /app/BOOT-INF/lib/
COPY --from=unzip /app/docker/org /app/org/
COPY --from=unzip /app/docker/META-INF /app/META-INF/
COPY --from=unzip /app/docker/BOOT-INF/classes /app/BOOT-INF/classes/


FROM amazoncorretto:11
