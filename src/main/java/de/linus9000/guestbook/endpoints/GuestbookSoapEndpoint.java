package de.linus9000.guestbook.endpoints;

import de.linus9000.guestbook.entities.Entry;
import de.linus9000.guestbook.repositories.EntryRepository;
import de.linus9000.guestbook.soap.AddGuestbookEntryRequest;
import de.linus9000.guestbook.soap.AddGuestbookEntryResponse;
import de.linus9000.guestbook.soap.DeleteGuestbookEntryRequest;
import de.linus9000.guestbook.soap.DeleteGuestbookEntryResponse;
import de.linus9000.guestbook.soap.GetAllGuestbookEntryRequest;
import de.linus9000.guestbook.soap.GetAllGuestbookEntryResponse;
import de.linus9000.guestbook.soap.GetGuestbookEntryRequest;
import de.linus9000.guestbook.soap.GetGuestbookEntryResponse;
import de.linus9000.guestbook.soap.GuestbookEntry;
import de.linus9000.guestbook.wrapper.GetAllGuestbookEntryResponseWrapper;
import de.linus9000.guestbook.wrapper.GuestbookEntryWrapper;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import org.springframework.ws.soap.SoapFaultException;

import java.util.ArrayList;
import java.util.List;

@CommonsLog
@Endpoint
public class GuestbookSoapEndpoint {
    private static final String NAMESPACE_URI = "http://linus9000.de/guestbook/soap";

    private EntryRepository entryRepository;

    @Autowired
    public GuestbookSoapEndpoint(EntryRepository countryRepository) {
        this.entryRepository = countryRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllGuestbookEntryRequest")
    @ResponsePayload
    public GetAllGuestbookEntryResponse getAllGuestbookEntryRequest(@RequestPayload GetAllGuestbookEntryRequest request) {

        GetAllGuestbookEntryResponseWrapper response = new GetAllGuestbookEntryResponseWrapper();

        List<Entry> entryList = (List<Entry>) entryRepository.findAll();
        List<GuestbookEntry> guestbookEntryList = new ArrayList<>();

        for (Entry currentEntry : entryList) {
            guestbookEntryList.add(new GuestbookEntryWrapper(currentEntry));
        }

        response.setAllEntries(guestbookEntryList);

        return response;
    }


    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getGuestbookEntryRequest")
    @ResponsePayload
    public GetGuestbookEntryResponse getGuestbookEntryRequest(@RequestPayload GetGuestbookEntryRequest request) {

        GetGuestbookEntryResponse response = new GetGuestbookEntryResponse();
        long id = request.getId();

        Entry entry = entryRepository.findById(id).orElse(null);
        if (entry == null) {

            String msg = String.format("ID nicht vorhanden: %s", id);
            log.info(msg);

            throw new SoapFaultException(msg);
        }

        response.setEntry(new GuestbookEntryWrapper(entry));


        return response;
    }


    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "addGuestbookEntryRequest")
    @ResponsePayload
    public AddGuestbookEntryResponse addGuestbookEntryRequest(@RequestPayload AddGuestbookEntryRequest request) {

        Entry entry = new Entry();
        entry.setContent(request.getEntry().getContent());
        entry.setUsername(request.getEntry().getUsername());

        Entry savedEntry = entryRepository.save(entry);

        AddGuestbookEntryResponse response = new AddGuestbookEntryResponse();
        response.setEntry(new GuestbookEntryWrapper(savedEntry));

        return response;
    }


    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteGuestbookEntryRequest")
    @ResponsePayload
    public DeleteGuestbookEntryResponse deleteGuestbookEntryRequest(@RequestPayload DeleteGuestbookEntryRequest request) {

        long id = request.getId();

        Entry entry = entryRepository.findById(id).orElse(null);
        if (entry == null) {

            String msg = String.format("Der Eintrag konnte nicht geloescht werden. (ID nicht vorhanden: %s)", id);
            log.info(msg);

            throw new SoapFaultException(msg);
        }

        GuestbookEntryWrapper entryWrapper = new GuestbookEntryWrapper(entry);
        entryRepository.deleteById(id);

        DeleteGuestbookEntryResponse response = new DeleteGuestbookEntryResponse();
        response.setEntry(entryWrapper);

        return response;
    }

}
