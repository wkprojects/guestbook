package de.linus9000.guestbook.pojo;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Data
public class GuestbookEntry {

    @NotNull
    @Size(min = 1)
    private String content;
    private String username;
}
