package de.linus9000.guestbook.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "guestbook")
public class ServiceProperties {

    private String description;
    private int entriesPerPage;
    private User user;

    @Data
    public static class User {
        private String file;
        private Ldap ldap;
    }

    @Data
    public static class Ldap {
        private String rootDn;
        private String domain;
        private String url;
    }
}
