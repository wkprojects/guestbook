package de.linus9000.guestbook.configuration;

import lombok.extern.apachecommons.CommonsLog;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;

import java.io.IOException;
import java.util.Properties;

@CommonsLog
@Configuration
public class UserConfiguration {


    @Bean
    public UserDetailsManager userDetailsService(ApplicationContext applicationContext, ServiceProperties serviceProperties) {

        log.info("Lade Benutzer aus: " + serviceProperties.getUser().getFile());
        Resource resource = applicationContext.getResource(serviceProperties.getUser().getFile());
        Properties properties;
        try {
            properties = PropertiesLoaderUtils.loadProperties(resource);
        } catch (IOException e) {
            log.fatal("Fehler beim Laden der " + serviceProperties.getUser().getFile());
            properties = new Properties();
        }

        return new InMemoryUserDetailsManager(properties);
    }
}