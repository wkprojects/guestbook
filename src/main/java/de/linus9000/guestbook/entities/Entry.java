package de.linus9000.guestbook.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
public class Entry {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String content;
    private String username;

}
