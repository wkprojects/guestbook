package de.linus9000.guestbook.controller;


import lombok.extern.apachecommons.CommonsLog;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller zur Veranschaulichung der unterschiedlichen Log-Level
 */
@CommonsLog
@RestController
public class GuestbookLoggingController {

    @GetMapping(value = "/log")
    public ResponseEntity logExampleMessage() {

        String logLevel = "OFF";

        if (log.isTraceEnabled()) {
            logLevel = "trace";
        }

        if (log.isDebugEnabled()) {
            logLevel = "debug";
        }

        if (log.isInfoEnabled()) {
            logLevel = "info";
        }

        if (log.isWarnEnabled()) {
            logLevel = "warn";
        }

        if (log.isErrorEnabled()) {
            logLevel = "error";
        }

        if (log.isFatalEnabled()) {
            logLevel = "fatal";
        }


        log.trace("LogLevel: Trace");
        log.debug("LogLevel: Debug");
        log.info("LogLevel: Info");
        log.warn("LogLevel: Warn");
        log.error("LogLevel: Error");
        log.fatal("LogLevel: Fatal");

        return ResponseEntity.ok("LogLevel: " + logLevel);
    }
}
