package de.linus9000.guestbook.controller;

import de.linus9000.guestbook.entities.Entry;
import de.linus9000.guestbook.pojo.GuestbookEntry;
import de.linus9000.guestbook.repositories.EntryRepository;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CommonsLog
@RestController
@RequestMapping(value = "/")
public class GuestbookController {

    private EntryRepository entryRepository;

    @Autowired
    public GuestbookController(EntryRepository entryRepository) {

        this.entryRepository = entryRepository;
    }


    @GetMapping
    public List<Entry> getAllEntries() {
        return (List<Entry>) entryRepository.findAll();
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getEntry(@PathVariable("id") Long id) {
        Entry entry = entryRepository.findById(id).orElse(null);
        if (entry == null) {
            log.info(String.format("ID nicht vorhanden: %s", id));
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(entry);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> deleteEntry(@PathVariable("id") Long id) {
        Entry entry = entryRepository.findById(id).orElse(null);
        if (entry == null) {
            log.info(String.format("ID nicht vorhanden: %s", id));
            return ResponseEntity.notFound().build();
        }

        entryRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @PostMapping
    public ResponseEntity<Entry> newEntry(@Validated @RequestBody GuestbookEntry input) {

        Entry entry = new Entry();
        entry.setContent(input.getContent());
        entry.setUsername(input.getUsername());

        entryRepository.save(entry);

        return ResponseEntity.ok(entry);
    }
}
