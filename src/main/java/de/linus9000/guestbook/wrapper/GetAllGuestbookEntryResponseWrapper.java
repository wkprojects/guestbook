package de.linus9000.guestbook.wrapper;

import de.linus9000.guestbook.soap.GetAllGuestbookEntryResponse;
import de.linus9000.guestbook.soap.GuestbookEntry;

import java.util.ArrayList;
import java.util.List;

public class GetAllGuestbookEntryResponseWrapper extends GetAllGuestbookEntryResponse {

    public void setAllEntries(List<GuestbookEntry> entryList) {
        this.allEntries = new ArrayList<>();
        this.allEntries.addAll(entryList);
    }
}
