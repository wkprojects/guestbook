package de.linus9000.guestbook.wrapper;

import de.linus9000.guestbook.entities.Entry;
import de.linus9000.guestbook.soap.GuestbookEntry;

public class GuestbookEntryWrapper extends GuestbookEntry {

    public GuestbookEntryWrapper(Entry entry) {

        this.id = entry.getId();
        this.username = entry.getUsername();
        this.content = entry.getContent();

    }

}
