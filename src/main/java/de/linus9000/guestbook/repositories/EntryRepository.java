package de.linus9000.guestbook.repositories;

import de.linus9000.guestbook.entities.Entry;
import org.springframework.data.repository.CrudRepository;

public interface EntryRepository extends CrudRepository<Entry, Long> {

}
