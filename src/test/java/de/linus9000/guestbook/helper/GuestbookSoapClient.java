package de.linus9000.guestbook.helper;

import de.linus9000.guestbook.soap.AddGuestbookEntryRequest;
import de.linus9000.guestbook.soap.AddGuestbookEntryResponse;
import de.linus9000.guestbook.soap.DeleteGuestbookEntryRequest;
import de.linus9000.guestbook.soap.DeleteGuestbookEntryResponse;
import de.linus9000.guestbook.soap.GetAllGuestbookEntryRequest;
import de.linus9000.guestbook.soap.GetAllGuestbookEntryResponse;
import de.linus9000.guestbook.soap.GetGuestbookEntryRequest;
import de.linus9000.guestbook.soap.GetGuestbookEntryResponse;
import de.linus9000.guestbook.soap.NewGuestbookEntry;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

public class GuestbookSoapClient extends WebServiceGatewaySupport {

    private String uri;


    public GuestbookSoapClient(int port) {
        this.uri = "http://localhost:" + port + "/guestbook/ws";
    }

    public GetAllGuestbookEntryResponse getAllGuestbookEntries() {

        GetAllGuestbookEntryRequest request = new GetAllGuestbookEntryRequest();

        return (GetAllGuestbookEntryResponse) getWebServiceTemplate().marshalSendAndReceive(this.uri, request);
    }

    public GetGuestbookEntryResponse getGuestbookEntry(long entryId) {

        GetGuestbookEntryRequest request = new GetGuestbookEntryRequest();
        request.setId(entryId);

        return (GetGuestbookEntryResponse) getWebServiceTemplate().marshalSendAndReceive(this.uri, request);
    }

    public AddGuestbookEntryResponse addGuestbookEntry(String username, String content) {

        NewGuestbookEntry entry = new NewGuestbookEntry();
        entry.setUsername(username);
        entry.setContent(content);

        AddGuestbookEntryRequest request = new AddGuestbookEntryRequest();
        request.setEntry(entry);

        return (AddGuestbookEntryResponse) getWebServiceTemplate().marshalSendAndReceive(this.uri, request);
    }

    public DeleteGuestbookEntryResponse deleteGuestbookEntry(long entryId) {

        DeleteGuestbookEntryRequest request = new DeleteGuestbookEntryRequest();
        request.setId(entryId);

        return (DeleteGuestbookEntryResponse) getWebServiceTemplate().marshalSendAndReceive(this.uri, request);
    }
}
