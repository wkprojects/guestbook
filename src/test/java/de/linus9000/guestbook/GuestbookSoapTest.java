package de.linus9000.guestbook;

import de.linus9000.guestbook.entities.Entry;
import de.linus9000.guestbook.helper.GuestbookSoapClient;
import de.linus9000.guestbook.repositories.EntryRepository;
import de.linus9000.guestbook.soap.AddGuestbookEntryResponse;
import de.linus9000.guestbook.soap.DeleteGuestbookEntryResponse;
import de.linus9000.guestbook.soap.GetAllGuestbookEntryResponse;
import de.linus9000.guestbook.soap.GetGuestbookEntryResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.ws.soap.client.SoapFaultClientException;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
class GuestbookSoapTest {

    @Autowired
    private EntryRepository entryRepository;

    private GuestbookSoapClient soapClient;

    public GuestbookSoapTest(@LocalServerPort int port) {
        this.soapClient = new GuestbookSoapClient(port);
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("de.linus9000.guestbook.soap");
        this.soapClient.setMarshaller(marshaller);
        this.soapClient.setUnmarshaller(marshaller);
    }

    @BeforeEach
    void beforeEach() {

        Entry entry = new Entry();
        entry.setUsername("Tim");
        entry.setContent("hallo Welt");

        entryRepository.save(entry);

        entry = new Entry();
        entry.setUsername("Tom");
        entry.setContent("hallo Mond");

        entryRepository.save(entry);
    }

    @AfterEach
    void afterEach() {
        entryRepository.deleteAll();
    }

    @Test
    void testGetAllEntries() {

        GetAllGuestbookEntryResponse response = this.soapClient.getAllGuestbookEntries();

        assertEquals("Tim", response.getAllEntries().get(0).getUsername());
        assertEquals("hallo Welt", response.getAllEntries().get(0).getContent());
    }

    @Test
    void testGetSecondEntry() {

        List<Entry> entries = (List<Entry>) entryRepository.findAll();
        Entry secondEntry = entries.get(1);
        GetGuestbookEntryResponse response = this.soapClient.getGuestbookEntry(secondEntry.getId());

        assertEquals("Tom", response.getEntry().getUsername());
        assertEquals(2, response.getEntry().getId());
    }

    @Test
    void testGetInvalidEntry() {

        assertThrows(SoapFaultClientException.class, () -> {
            this.soapClient.getGuestbookEntry(-3);
        });
    }


    @Test
    void testDeleteFirstEntry() {

        List<Entry> entries = (List<Entry>) entryRepository.findAll();
        Entry secondEntry = entries.get(0);
        DeleteGuestbookEntryResponse response = this.soapClient.deleteGuestbookEntry(secondEntry.getId());

        assertEquals("Tim", response.getEntry().getUsername());
    }

    @Test
    void testDeleteInvalidEntry() {

        assertThrows(SoapFaultClientException.class, () -> {
            this.soapClient.deleteGuestbookEntry(-3);
        });
    }


    @Test
    void testCreateBrokenEntry() {

        assertThrows(SoapFaultClientException.class, () -> {
            this.soapClient.addGuestbookEntry(null, null);
        });
    }


    @Test
    void testCreateCorrectEntry() {

        AddGuestbookEntryResponse response = this.soapClient.addGuestbookEntry("Hans", "Schönes Wetter heute!");

        assertEquals("Hans", response.getEntry().getUsername());
    }
}
