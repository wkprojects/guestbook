package de.linus9000.guestbook;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class MonitoringTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    void health_unauthenticated() {
        try {
            mockMvc.perform(
                    get("/actuator/health"))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.details").doesNotExist());
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    @WithMockUser(roles = {"MONITOR"})
    void health_properly_authenticated() {
        try {
            mockMvc.perform(
                    get("/actuator/health"))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.details").exists());
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    @WithMockUser(roles = {"TEST"})
    void health_improperly_authenticated() {
        try {
            mockMvc.perform(
                    get("/actuator/health"))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.details").doesNotExist());
        } catch (Exception e) {
            fail();
        }
    }

}
