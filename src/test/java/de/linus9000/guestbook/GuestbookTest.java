package de.linus9000.guestbook;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.linus9000.guestbook.entities.Entry;
import de.linus9000.guestbook.pojo.GuestbookEntry;
import de.linus9000.guestbook.repositories.EntryRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
class GuestbookTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private EntryRepository entryRepository;

    @BeforeEach
    void beforeEach() {

        Entry entry = new Entry();
        entry.setUsername("Tim");
        entry.setContent("hallo Welt");

        entryRepository.save(entry);

        entry = new Entry();
        entry.setUsername("Tom");
        entry.setContent("hallo Mond");

        entryRepository.save(entry);
    }

    @AfterEach
    void afterEach() {
        entryRepository.deleteAll();
    }

    private static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void testGetAllEntries() throws Exception {
        this.mockMvc.perform(get("/")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    void testGetFirstEntry() throws Exception {
        List<Entry> entries = (List<Entry>) entryRepository.findAll();
        Entry firstEntry = entries.get(0);
        this.mockMvc.perform(get("/" + firstEntry.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.username", is("Tim")));

    }

    @Test
    void testGetInvalidEntry() throws Exception {

        this.mockMvc.perform(get("/" + -3))
                .andExpect(status().isNotFound());

    }

    @Test
    void testDeleteFirstEntry() throws Exception {
        List<Entry> entries = (List<Entry>) entryRepository.findAll();
        Entry firstEntry = entries.get(0);
        this.mockMvc.perform(delete("/" + firstEntry.getId()))
                .andExpect(status().is2xxSuccessful());

    }

    @Test
    void testDeleteInvalidEntry() throws Exception {

        this.mockMvc.perform(delete("/" + -3))
                .andExpect(status().isNotFound());
    }

    @Test
    void testCreateBrokenEntry() throws Exception {
        GuestbookEntry entry = new GuestbookEntry();
        String bodyJson = asJsonString(entry);
        this.mockMvc.perform(post("/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testCreateCorrectEntry() throws Exception {

        GuestbookEntry entry = new GuestbookEntry();
        entry.setUsername("Fritz");
        entry.setContent("Hallo!");

        String bodyJson = asJsonString(entry);
        this.mockMvc.perform(post("/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.username", is("Fritz")));

        assertEquals(3, entryRepository.count());
    }
}
